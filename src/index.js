
/**
	Autor: Juan Camilo Mendoza
	Maintainer: Juan Camilo Mendoza
	Last modification: Juan Camilo Mendoza
*/
//REACT
import React from 'react'
import ReactDOM from 'react-dom'
import {HashRouter} from 'react-router-dom';
import Favicon from 'react-favicon';

//CONTEXT
import {MProvider} from './pwa/provider/Provider'

//COMPONENTS
import Welcome  from './pwa/welcome/Welcome'

//STYLE
import style from './Main.scss';

//IMGS
import fav from './pwa/images/icon.png';

//SW
(function () {
  if ('serviceWorker' in navigator) {
    navigator
      .serviceWorker
      .register('service-worker.js')
      .then(() => console.log('Service Worker registered successfully.'))
      .catch(error => console.log('Service Worker registration failed:', error));
  }
})();

const appDOM = document.getElementById("app")

const App = () => {
  return (
    <HashRouter>
       <div className="App-box">
        <Favicon url={fav}/>
        <MProvider>
          
          <Welcome/>
        </MProvider>
      </div>
    </HashRouter>
  )
}

ReactDOM.render(
  <App/>, appDOM)