workbox.core.setCacheNameDetails({
  prefix: 'MILO-CV',
  suffix: 'v1',
  precache: 'precache',
  runtime: 'run-time',
  googleAnalytics: 'ga',
});

const cacheName = 'my-cache';
const expirationManager = new workbox.expiration.CacheExpiration(
  cacheName,
  {
    maxAgeSeconds: 24 * 60 * 60,
    maxEntries: 20,
  }
);

workbox.precaching.precacheAndRoute(self.__precacheManifest || [])