//REACT
import React from 'react';

//STYLE
import './bar.scss'

const Bar = (props) => {
    let _width = { width : props.number*3.8}
    let _width_two = { width : (100 - props.number)*(3.8)}
    return (
        <div className="Bar-box"> 
            <p className="Bar-name">{props.name}</p>
            <div className="Bar-setion-color">
                <label className="Bar-color Bar_orange" style={_width}/>
                <label className="Bar-color Bar_white" style={_width_two}/> 
            </div>
            {/*<p className="Bar-number">{props.number}%</p>*/}
        </div> 
    )
};

export default Bar;