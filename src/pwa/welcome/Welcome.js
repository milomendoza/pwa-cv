/**
	Autor: Juan Camilo Mendoza
	Maintainer: Juan Camilo Mendoza
	Last modification: Juan Camilo Mendoza
*/

//REACT
import React from 'react'

//COMPONENTS
import Bar from '../Bar/Bar'

//STYLE
import './welcome.scss'

//CONTEXT
import {MContext} from '../provider/Provider'

//IMGS
import me from '././../images/me.jpg';
import smartphone from '././../images/smartphone-call.svg';
import mail from '././../images/mail.svg';
import linked from '././../images/linked-in-logo.svg';

import football from '././../images/football.svg';
import dancer_with from '././../images/dancer-with-music.svg';
import fast from '././../images/fast-food.svg';

const Welcome = () => {

    return(
        <MContext.Consumer>
            {(context) => (
                <React.Fragment>
                    <div className="Welcome-box">
                        <div className="Left-wrap">
                            <div className="Left-header">
                                <img className="My-img" src={me} alt="me"/>
                                <p className="My-name">
                                   {context.state.name}
                                </p>
                                <p className="My-profession">
                                    {context.state.profession}
                                </p>
                            </div>
                            <div className="Left-content">
                                <div className="Left-info_box">
                                    <div className="Left-title">
                                        {context.state.personal_profile._title}
                                    </div>
                                    <div className="Left-text">
                                        <div className="Paragraph">I am a Telematics Engineer with interest in working in areas such as <div className="Paragraph-bold">web development </div> Technology-based company management and ICT solutions integration. Experience in web development, ICT project management and technology management.</div>
                                    </div>
                                    <div className="Left-text">
                                        <div className="Paragraph">A leader person, with teamwork skills, excellent interpersonal relationships and ease and willingness to learn.</div>
                                    </div>
                                </div>
                                <div className="Left-info_box">
                                    <div className="Left-title">
                                        {context.state.contact._title}
                                    </div>
                                    <div className="Left-text Left-text-contact">
                                        <img src={smartphone} className="Icon Special-margin" alt="smartphone" />
                                        <a className="Paragraph" target="_blank" href={"https://api.whatsapp.com/send?phone=57"+context.state.contact.phone+"&text=Holaa%20Milo!"}> {context.state.contact.phone}</a>
                                    </div>
                                    <div className="Left-text Left-text-contact">
                                        <img src={mail} className="Icon Special-margin" alt="mail" />
                                        <a className="Paragraph" target="_blank" href={"mailto:"+context.state.contact.mail}> {context.state.contact.mail}</a>
                                    </div>
                                    <div className="Left-text Left-text-contact">
                                        <img src={linked} className="Icon Special-size Special-margin" alt="linked" />
                                        <a className="Paragraph" target="_blank" href={context.state.contact.linkedin}> linkedin/MILO </a>
                                    </div>
                                </div>
                                <div className="Left-info_box">
                                    <div className="Left-title">
                                        {context.state.lenguages._title}
                                    </div>
                                    <div className="Left-text Left-text-contact">
                                        {context.state.lenguages.options.map((element, i) => { 
                                            return <Bar  name={element.name} number={element.number} key={i} /> 
                                        })}
                                    </div>
                                </div>
                                <div className="Left-info_box">
                                    <div className="Left-title">
                                        {context.state.skills._title}
                                    </div>
                                    <div className="Left-skills">
                                        {context.state.skills.options.map((element, i) => { 
                                            return( 
                                                <div className="Left-text" key={i}>
                                                    <div className="Left-skills-name Left-skills-title">
                                                        {element.name}
                                                    </div>
                                                    <div className="Left-skills-name">
                                                        ( {element.list} )
                                                    </div>
                                                </div>  
                                            )
                                        })}
                                    </div>
                                </div>
                                <div className="Left-info_box">
                                    <div className="Left-title">
                                        {context.state.hobbies._title}
                                    </div>
                                    <div className="Left-text">
                                        {context.state.hobbies.options.map((element, i) => { 
                                            return(
                                                <div key={i}>
                                                    { 
                                                        element.name == 'soccer' &&
                                                        <img className="Icon-hobbies" src={football} alt="football"/>
                                                    }
                                                    {
                                                        element.name == 'dance' &&
                                                        <img className="Icon-hobbies" src={dancer_with} alt="dancer_with"/>
                                                    }
                                                    {
                                                        element.name === 'fastfood' &&
                                                        <img className="Icon-hobbies" src={fast} alt="fast"/>
                                                    }
                                                </div>
                                            ) 
                                        })}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="Left-wrap">
                        </div>
                    </div>
                </React.Fragment>
            )}
        </MContext.Consumer>
    )
}

export default Welcome