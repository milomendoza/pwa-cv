/**
	Autor: Juan Camilo Mendoza
	Maintainer: Juan Camilo Mendoza
	Last modification: Juan Camilo Mendoza
*/

//REACT
import React, { Component } from 'react';
import { withRouter } from 'react-router';

//CONTEXT
const MyContext = React.createContext()

class Provider extends Component{

    
    state = {
        name:'Juan Camilo Mendoza Balanta',
        profession:'Telematics Engineer',
        personal_profile: {
            _title:'personal profile',
        },
        contact: {
            _title:'contact',
            phone:'3128544398',
            mail:'juancamilomendozabalanta.jcmb@gmail.com',
            linkedin:'https://co.linkedin.com/in/juancamilomendozabalanta'
        },
        lenguages:{
            _title:'lenguages',
            options:[
                {
                    name:'English',
                    number:70
                }
            ]
        },
        skills:{
            _title:'skills',
            options:[
                {
                    name:'markup',
                    list:'html, pug, jsx'
                },
                {
                    name:'css',
                    list:'sass, stylus'
                },
                {
                    name:'bundlers',
                    list:'gulp, webpack'
                },
                {
                    name:'js',
                    list:'jquery, es5/6'
                },
                {
                    name:'frameworks',
                    list:'jquery, es5/6'
                },
                {
                    name:'js',
                    list:'AngularJS, React'
                },
                {
                    name:'version control',
                    list:'git'
                }
            ]
        },
        hobbies:{
            _title:'hobbies',
            options:[
                {
                    name:'soccer',
                },
                {
                    name:'dance',
                },
                {
                    name:'fastfood',
                }
            ]
        },
        references:{
            _title:'references',
            options:[
                {
                    name:'Alexander Larrahondo',
                    profession:'IP Engineer and Packet Core',
                    company:'Movistar',
                    phone:'3162100221'
                },
                {
                    name:'William Nauffal',
                    profession:'Business analyst semi senior advance',
                    company:'Globant',
                    phone:'3173032780'
                }
            ]
        }
    }

    componentDidMount(){
    }


    

    render(){
        return(
            <MyContext.Provider value={{state: this.state}}>
                {this.props.children}
            </MyContext.Provider>
        )
    }
    
}

export const MProvider = withRouter(Provider)
export const  MContext = MyContext