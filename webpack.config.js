const path = require('path');
const HtmlWebPackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CopyWebpackPlugin = require('copy-webpack-plugin');
const WorkboxPlugin = require('workbox-webpack-plugin');
const WebpackPwaManifest = require('webpack-pwa-manifest');


module.exports = {
  mode: 'development',
  devServer: {
    host: 'localhost',
    port: 3002
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader"
        }
      }, {
        test: /\.html$/,
        use: [
          {
            loader: "html-loader",
            options: {
              minimize: false
            }
          }
        ]
      }, {
        test: /\.css$/,
        use: [MiniCssExtractPlugin.loader, "css-loader"]
      },
      //MENDOZA
      {
        test: /\.scss$/,
        use:  [ 'style-loader', MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader']
      },
      { test: /\.(jpeg|jpg|png|woff|woff2|eot|ttf|svg)$/, 
        loader: 'url-loader?limit=100000' 
      },
    ]
  },
  plugins: [
    new HtmlWebPackPlugin({template: "./src/index.html", filename: "./index.html"}),
    new MiniCssExtractPlugin({filename: "[name].css", chunkFilename: "[id].css"}),
    new CopyWebpackPlugin([
      {
        from: 'src/pwa'
      } // define the path of the files to be copied
    ]),
    new WorkboxPlugin.InjectManifest({
      swSrc: './src/service-worker.js',
      swDest: 'service-worker.js'
    }),
    // new WorkboxPlugin.GenerateSW({   runtimeCaching: [     {       urlPattern:
    // /images/,       handler: 'cacheFirst'     }, {       urlPattern: new
    // RegExp('^https://fonts.(?:googleapis|gstatic).com/(.*)'),       handler:
    // 'cacheFirst'     }, {       urlPattern: /.*/,       handler: 'networkFirst' }
    //   ] }),
    new WebpackPwaManifest({
      name: "I'MILO",
      short_name: "I'MILO",
      description: "HI...I am a Telematics Engineer with interest in working in areas such as WEB DEVELOPMENT",
      background_color: '#ffffff',
      theme_color: '#000000',
      start_url: '/',
      inject: true,
      ios: true,
      icons: [
        {
          src: path.resolve('src/pwa/images/icon.png'),
          sizes: [96,128,192,256,384,512
          ],
          destination: path.join('dist', 'icons')
        },
        {
          src: path.resolve('src/pwa/images/icon.png'),
          sizes: 180,
          destination: path.join('dist', 'icons'),
          ios: 'startup'
        },
        {
          src: path.resolve('src/pwa/images/icon.png'),
          sizes: [120, 152, 167, 180, 1024],
          destination: path.join('dist', 'icons'),
          ios: true
        },
      ]
    }),

  ]
};